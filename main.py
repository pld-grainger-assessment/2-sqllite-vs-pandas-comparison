import pandas as pd

url = 'https://s3-us-west-2.amazonaws.com/pcadsassessment/parking_citations.corrupted.csv'
df = pd.read_csv(url)

get_uncorrupted_df = lambda df: df[~df['Make'].isnull()]

def calculate_top_25_most_common_makes(df):
    df_with_make = get_uncorrupted_df(df)
    s_num_citations_per_make = df_with_make[df_with_make['Make'] != 'OTHR']['Make'].value_counts()
    return s_num_citations_per_make[:25].keys().tolist()

def calculate_most_common_color_per_make(df):
    df_with_make = get_uncorrupted_df(df)
    return df_with_make.groupby(['Make', 'Color']).apply(len).groupby(level=0).idxmax().apply(lambda key: key[-1])


from functools import partial
def try_to_convert(val, method, default=None):
    try:
        return method(val)
    except:
        return default
try_to_convert_to_int = partial(try_to_convert, method=int)
def convert_to_datetime(s_datetime_info):
    issue_hour = try_to_convert_to_int(s_datetime_info['issue_hour'], default=23)
    issue_minute = try_to_convert_to_int(s_datetime_info['issue_minute'], default=59)
    return s_datetime_info['Issue Date'].replace(hour=issue_hour, minute=issue_minute )

def clean_issue_time(df):
    col = 'Issue time'
    col_name_new = col.lower().replace('time', '').strip().replace(' ', '_')

    df = df[~df['Make'].isnull()]

    df[f'{col_name_new}_minute'] = df[col].dropna().astype(int).astype(str).str.slice(
        start=-2).apply(
        try_to_convert_to_int)
    df[f'{col_name_new}_hour'] = df[col].dropna().astype(int).astype(str).str.slice(stop=-2).apply(
        try_to_convert_to_int)
    df['Issue Date'] = pd.to_datetime(df['Issue Date'])
    df['issue_datetime'] = df[['Issue Date', 'issue_minute', 'issue_hour']].apply(
        convert_to_datetime, axis=1)

    return df

def find_first_ticket_for_each_make(df):
    df_with_make = get_uncorrupted_df(df)
    df_with_make = clean_issue_time(df_with_make)
    return df_with_make.sort_values(by='issue_datetime', ascending=True).groupby('Make').first()