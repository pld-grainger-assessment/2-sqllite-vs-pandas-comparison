import pytest
from main import main
from cases import cases

class Test_Results(object):
    def run_tests_for_case(self, case, method, verbose=False):
        '''
        Run the all the tests for a single case

        case: The inputs/outputs used to verify the response to the question
        method: callable: The method used to get the results to verify

        '''
        assert type(case) is dict
        if verbose:
            print("\n")
            print('-'*88)
            print(f'Testing method: {method.__name__} against case: {case}')
        result = method(**case.get('inputs', {}))
        if verbose:
            print(f'result: {result}')
        assert result == case.get('outputs')

    @pytest.mark.parametrize("case", cases)
    def test_main(self, case):
        '''
        Run the tests for this question against the main method

        case: dict<str:Any>: The case to test
        '''
        self.run_tests_for_case(case, method=main)