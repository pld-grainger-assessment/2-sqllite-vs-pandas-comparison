help: ## Show this help.
	@awk 'BEGIN {FS = ":.?## "} /^[a-zA-Z_-]*:.?## / {sub("\\n", sprintf("\n%22c", " ") $$2); printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

prep_env: ## Ensure the virtual environment is created
	pip install virtualenv --user
	virtualenv venv -p `which python3`
	source venv/bin/activate && pip install -r requirements.txt

test: ## Run the scripts to setup this computer
	source activate coding_practice && pytest
