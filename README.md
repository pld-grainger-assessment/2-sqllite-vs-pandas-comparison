# SQLLite vs Pandas Comparison
Your business partner is interested in using either pandas as sqlite to calculate some statistics on this dataset.  

Set up a sqlite database to do a few comparisons with pandas.  Your business partner is interested in both run time and ease of use. 
    
Don't get too deep - let's just find out which is faster and which is easier to code up.

Do this analysis on the uncorrupted component of the data only.


Compare sqlite vs pandas on the following tasks:

	a) calculate top 25 most common 'makes'
        	b) calculate most common 'Color' for each 'Make' 
        	c) find the first ticket issued for each 'Make'


Choose either pandas or sqlite to answer the following question:

"Is an out-of-state license plate more likely to be expired at the time of recieving a ticket than an in-state license plate?"

## Getting Started
The pandas versions of the questions are answered in the `exploration/pandas_questions` notebook

Notes
 - Pandas version much faster to code up
 - Have not completed the SQLLite version of problems yet